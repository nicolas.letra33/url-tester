<?php

$filename = $argv[1];
$lines = file($filename);
$count = 0;

echo("URL non valides :\n\n");

foreach($lines as $url){
    $url = str_replace(["\n","\r"],["",""],$url);           // supprime les caractères de saut de ligne et retour chariot
    $code = substr(get_headers($url)[0],9,3);

    if($code != '200'){
        echo($code." - ".$url."\n");
        $count += 1;
    }
}

echo("\n".$count." url non valide(s)\n");
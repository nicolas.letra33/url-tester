# URL Tester

## I - Présentation

Ce projet permet de tester une liste d'url et d'identifier les url qui présentent des erreurs dans leur code de statut http (comme par exemple l'erreur 404).  

La liste d'url à tester doit se trouver dans un fichier .txt qui sera utilisé comme argument du script. Il doit y avoir une url par ligne pour que le script puisse les lire et les tester correctement.  

Lorsqu'une des url de la liste renvoie un code de statut différent de 200, le code erreur ainsi que l'url non fonctionnelle sont affichées.  

A la fin du script, le nombre d'url non fonctionnelles s'affiche. 

## II - Utilisation

>Pour être utilisé ce projet nécessite php.

Une fois placé dans le projet, il suffit de créer un fichier .txt contenant la liste d'url qu'on veut tester et de l'uitliser comme argument tel que :  

>php url_tester.php <**nom du fichier txt**>

### Exemple :

Dans ce projet se trouve un fichier url_list.txt contenant 100 url recrées suite à une refonte du site du ministère de l'enseignement supérieur.  

Lorsqu'on appelle le script avec ce fichier comme argument on a en retour toutes les url non fonctionelles et le compte total comme ci dessous :  

![retour script](https://www.pixenli.com/image/Wr7whfVV)